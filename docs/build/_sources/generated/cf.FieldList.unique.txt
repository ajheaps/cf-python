cf.FieldList.unique
===================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.FieldList.unique