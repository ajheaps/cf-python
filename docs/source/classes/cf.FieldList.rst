.. currentmodule:: cf
.. default-role:: obj

cf.FieldList
============

.. autoclass:: cf.FieldList
   :no-members:
   :no-inherited-members:

FieldList CF Properties
-----------------------
 
.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.FieldList.add_offset
   ~cf.FieldList.calendar
   ~cf.FieldList.cell_methods
   ~cf.FieldList.comment
   ~cf.FieldList.Conventions
   ~cf.FieldList._FillValue
   ~cf.FieldList.flag_masks
   ~cf.FieldList.flag_meanings
   ~cf.FieldList.flag_values
   ~cf.FieldList.history
   ~cf.FieldList.institution
   ~cf.FieldList.leap_month
   ~cf.FieldList.leap_year
   ~cf.FieldList.long_name
   ~cf.FieldList.missing_value
   ~cf.FieldList.month_lengths
   ~cf.FieldList.references
   ~cf.FieldList.scale_factor
   ~cf.FieldList.source
   ~cf.FieldList.standard_error_multiplier
   ~cf.FieldList.standard_name
   ~cf.FieldList.title
   ~cf.FieldList.units
   ~cf.FieldList.valid_max
   ~cf.FieldList.valid_min
   ~cf.FieldList.valid_range


FieldList attributes
--------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.FieldList.ancillary_variables
   ~cf.FieldList.array
   ~cf.FieldList.attributes
   ~cf.FieldList.data
   ~cf.FieldList.day
   ~cf.FieldList.domain
   ~cf.FieldList.dtarray
   ~cf.FieldList.dtvarray
   ~cf.FieldList.dtype
   ~cf.FieldList.hardmask
   ~cf.FieldList.hour
   ~cf.FieldList.isscalar
   ~cf.FieldList.Flags
   ~cf.FieldList.mask
   ~cf.FieldList.minute
   ~cf.FieldList.month
   ~cf.FieldList.ndim
   ~cf.FieldList.properties
   ~cf.FieldList.second
   ~cf.FieldList.subspace
   ~cf.FieldList.shape
   ~cf.FieldList.size
   ~cf.FieldList.sum
   ~cf.FieldList.unique
   ~cf.FieldList.Units
   ~cf.FieldList.varray
   ~cf.FieldList.year

FieldList domain item methods
-----------------------------

The following methods share a common API for the selection of domain
items, i.e. axes, dimension coordinates, auxiliary coordinates, cell
measures and coordinate references.
   
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :Template: method.rst

   ~cf.FieldList.aux
   ~cf.FieldList.auxs
   ~cf.FieldList.axes
   ~cf.FieldList.axes_sizes
   ~cf.FieldList.axis
   ~cf.FieldList.axis_name
   ~cf.FieldList.axis_size
   ~cf.FieldList.coord
   ~cf.FieldList.coords
   ~cf.FieldList.cyclic
   ~cf.FieldList.dim
   ~cf.FieldList.dims
   ~cf.FieldList.iscyclic
   ~cf.FieldList.item
   ~cf.FieldList.item_axes
   ~cf.FieldList.items
   ~cf.FieldList.items_axes
   ~cf.FieldList.measure
   ~cf.FieldList.measures
   ~cf.FieldList.period
   ~cf.FieldList.promote
   ~cf.FieldList.ref
   ~cf.FieldList.refs
   ~cf.FieldList.remove_axes
   ~cf.FieldList.remove_axis
   ~cf.FieldList.remove_item
   ~cf.FieldList.remove_items

FieldList methods
-----------------
   
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.FieldList.allclose
   ~cf.FieldList.anchor
   ~cf.FieldList.asdatetime
   ~cf.FieldList.asreftime
   ~cf.FieldList.binary_mask
   ~cf.FieldList.ceil
   ~cf.FieldList.chunk
   ~cf.FieldList.clip
   ~cf.FieldList.close
   ~cf.FieldList.collapse
   ~cf.FieldList.copy
   ~cf.FieldList.cos
   ~cf.FieldList.data_axes
   ~cf.FieldList.datum
   ~cf.FieldList.delattr
   ~cf.FieldList.delprop
   ~cf.FieldList.dump
   ~cf.FieldList.equals
   ~cf.FieldList.equivalent
   ~cf.FieldList.equivalent_data
   ~cf.FieldList.equivalent_domain
   ~cf.FieldList.expand_dims
   ~cf.FieldList.fill_value
   ~cf.FieldList.flip
   ~cf.FieldList.floor
   ~cf.FieldList.getattr
   ~cf.FieldList.getprop
   ~cf.FieldList.hasattr
   ~cf.FieldList.hasprop
   ~cf.FieldList.identity
   ~cf.FieldList.indices
   ~cf.FieldList.insert_aux
   ~cf.FieldList.insert_axis
   ~cf.FieldList.insert_data
   ~cf.FieldList.insert_dim
   ~cf.FieldList.insert_measure
   ~cf.FieldList.insert_ref
   ~cf.FieldList.iter
   ~cf.FieldList.mask_invalid
   ~cf.FieldList.max
   ~cf.FieldList.mean
   ~cf.FieldList.match
   ~cf.FieldList.method
   ~cf.FieldList.mid_range
   ~cf.FieldList.min
   ~cf.FieldList.name
   ~cf.FieldList.override_units
   ~cf.FieldList.range
   ~cf.FieldList.rint
   ~cf.FieldList.roll
   ~cf.FieldList.sample_size
   ~cf.FieldList.sd
   ~cf.FieldList.select
   ~cf.FieldList.setattr
   ~cf.FieldList.setcyclic
   ~cf.FieldList.setprop
   ~cf.FieldList.sin
   ~cf.FieldList.sort
   ~cf.FieldList.squeeze
   ~cf.FieldList.subspace
   ~cf.FieldList.sum
   ~cf.FieldList.transpose
   ~cf.FieldList.trunc
   ~cf.FieldList.unsqueeze
   ~cf.FieldList.var
   ~cf.FieldList.weights
   ~cf.FieldList.where

FieldList list-like methods
---------------------------

These methods provide functionality similar to that of a built-in
:py:obj:`list`.

Undocumented methods behave exactly as their counterparts in a
built-in :py:obj:`list`.

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.FieldList.append
   ~cf.FieldList.count
   ~cf.FieldList.extend
   ~cf.FieldList.index
   ~cf.FieldList.insert
   ~cf.FieldList.pop
   ~cf.FieldList.reverse
   ~cf.FieldList.sort

FieldList arithmetic and comparison operations
----------------------------------------------

Any arithmetic, bitwise or comparison operation is applied
independently to each field element, so all of :ref:`operators defined
for a field <Arithmetic-and-comparison>` are allowed.

In particular, the usual list-like operator behaviours do not
apply. For example, the ``+`` operator will concatenate two built-in
lists, but adding ``2`` to a field list will add ``2`` to the data
array of each of its fields.
