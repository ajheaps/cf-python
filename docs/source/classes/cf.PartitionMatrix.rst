.. currentmodule:: cf
.. default-role:: obj

cf.PartitionMatrix
==================

.. autoclass:: cf.PartitionMatrix
   :no-members:
   :no-inherited-members:

PartitionMatrix attributes
--------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~PartitionMatrix.ndim
   ~PartitionMatrix.shape
   ~PartitionMatrix.size
   
PartitionMatrix methods
-----------------------

Undocumented methods behave exactly as their counterparts in a
built-in list.
     
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.PartitionMatrix.add_partitions
   ~cf.PartitionMatrix.change_axis_names
   ~cf.PartitionMatrix.copy
   ~cf.PartitionMatrix.expand_dims
   ~cf.PartitionMatrix.ndenumerate
   ~cf.PartitionMatrix.partition_boundaries
   ~cf.PartitionMatrix.set_location_map
   ~cf.PartitionMatrix.squeeze
   ~cf.PartitionMatrix.swapaxes
   ~cf.PartitionMatrix.transpose
