.. currentmodule:: cf
.. default-role:: obj


cf.DimensionCoordinate
======================

.. autoclass:: cf.DimensionCoordinate
   :no-members:
   :no-inherited-members:

DimensionCoordinate CF properties
---------------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.DimensionCoordinate.add_offset
   ~cf.DimensionCoordinate.axis
   ~cf.DimensionCoordinate.calendar
   ~cf.DimensionCoordinate.comment
   ~cf.DimensionCoordinate._FillValue
   ~cf.DimensionCoordinate.history
   ~cf.DimensionCoordinate.leap_month
   ~cf.DimensionCoordinate.leap_year
   ~cf.DimensionCoordinate.long_name
   ~cf.DimensionCoordinate.missing_value
   ~cf.DimensionCoordinate.month_lengths
   ~cf.DimensionCoordinate.positive
   ~cf.DimensionCoordinate.scale_factor
   ~cf.DimensionCoordinate.standard_name
   ~cf.DimensionCoordinate.units
   ~cf.DimensionCoordinate.valid_max
   ~cf.DimensionCoordinate.valid_min
   ~cf.DimensionCoordinate.valid_range


DimensionCoordinate attributes
------------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.DimensionCoordinate.array
   ~cf.DimensionCoordinate.attributes
   ~cf.DimensionCoordinate.bounds
   ~cf.DimensionCoordinate.cellsize
   ~cf.DimensionCoordinate.ctype
   ~cf.DimensionCoordinate.data
   ~cf.DimensionCoordinate.day
   ~cf.DimensionCoordinate.dtarray
   ~cf.DimensionCoordinate.dtvarray
   ~cf.DimensionCoordinate.dtype
   ~cf.DimensionCoordinate.hour
   ~cf.DimensionCoordinate.hardmask
   ~cf.DimensionCoordinate.hasbounds
   ~cf.DimensionCoordinate.isauxiliary
   ~cf.DimensionCoordinate.isdimension
   ~cf.DimensionCoordinate.isscalar
   ~cf.DimensionCoordinate.lower_bounds
   ~cf.DimensionCoordinate.mask
   ~cf.DimensionCoordinate.minute
   ~cf.DimensionCoordinate.month
   ~cf.DimensionCoordinate.ndim
   ~cf.DimensionCoordinate.properties
   ~cf.DimensionCoordinate.second
   ~cf.DimensionCoordinate.shape
   ~cf.DimensionCoordinate.size
   ~cf.DimensionCoordinate.subspace
   ~cf.DimensionCoordinate.T
   ~cf.DimensionCoordinate.unique
   ~cf.DimensionCoordinate.Units
   ~cf.DimensionCoordinate.upper_bounds
   ~cf.DimensionCoordinate.varray
   ~cf.DimensionCoordinate.X
   ~cf.DimensionCoordinate.Y
   ~cf.DimensionCoordinate.year
   ~cf.DimensionCoordinate.Z


DimensionCoordinate methods
---------------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.DimensionCoordinate.asauxiliary
   ~cf.DimensionCoordinate.asdatetime
   ~cf.DimensionCoordinate.asdimension
   ~cf.DimensionCoordinate.asreftime
   ~cf.DimensionCoordinate.binary_mask
   ~cf.DimensionCoordinate.chunk
   ~cf.DimensionCoordinate.clip
   ~cf.DimensionCoordinate.close
   ~cf.DimensionCoordinate.contiguous
   ~cf.DimensionCoordinate.copy
   ~cf.DimensionCoordinate.cos
   ~cf.DimensionCoordinate.datum
   ~cf.DimensionCoordinate.delattr
   ~cf.DimensionCoordinate.delprop
   ~cf.DimensionCoordinate.direction
   ~cf.DimensionCoordinate.dump
   ~cf.DimensionCoordinate.equals
   ~cf.DimensionCoordinate.expand_dims
   ~cf.DimensionCoordinate.fill_value
   ~cf.DimensionCoordinate.flip
   ~cf.DimensionCoordinate.get_bounds
   ~cf.DimensionCoordinate.getattr
   ~cf.DimensionCoordinate.getprop
   ~cf.DimensionCoordinate.hasattr
   ~cf.DimensionCoordinate.hasprop
   ~cf.DimensionCoordinate.identity
   ~cf.DimensionCoordinate.insert_bounds
   ~cf.DimensionCoordinate.insert_data
   ~cf.DimensionCoordinate.match
   ~cf.DimensionCoordinate.name
   ~cf.DimensionCoordinate.override_units
   ~cf.DimensionCoordinate.period
   ~cf.DimensionCoordinate.select
   ~cf.DimensionCoordinate.setattr
   ~cf.DimensionCoordinate.setprop
   ~cf.DimensionCoordinate.sin
   ~cf.DimensionCoordinate.squeeze
   ~cf.DimensionCoordinate.transpose
   ~cf.DimensionCoordinate.where
