cf.Field.remove_item
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.remove_item