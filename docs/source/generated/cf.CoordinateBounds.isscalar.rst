cf.CoordinateBounds.isscalar
============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.isscalar