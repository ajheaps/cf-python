cf.Domain.remove_axis
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.remove_axis