cf.Variable.select
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.select