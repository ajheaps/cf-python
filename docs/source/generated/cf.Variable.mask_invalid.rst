cf.Variable.mask_invalid
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.mask_invalid