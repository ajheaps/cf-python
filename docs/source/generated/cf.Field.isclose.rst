cf.Field.isclose
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.isclose