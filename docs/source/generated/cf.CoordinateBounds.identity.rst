cf.CoordinateBounds.identity
============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.identity