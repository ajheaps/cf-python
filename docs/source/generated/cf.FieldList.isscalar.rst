cf.FieldList.isscalar
=====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.FieldList.isscalar