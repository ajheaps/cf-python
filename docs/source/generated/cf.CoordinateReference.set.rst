cf.CoordinateReference.set
==========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateReference.set