cf.Coordinate.scale_factor
==========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.scale_factor