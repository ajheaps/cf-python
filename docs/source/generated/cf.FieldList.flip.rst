cf.FieldList.flip
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.flip