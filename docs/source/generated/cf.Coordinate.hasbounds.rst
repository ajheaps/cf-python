cf.Coordinate.hasbounds
=======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.hasbounds