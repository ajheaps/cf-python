cf.Variable.delattr
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.delattr