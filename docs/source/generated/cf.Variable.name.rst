cf.Variable.name
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.name