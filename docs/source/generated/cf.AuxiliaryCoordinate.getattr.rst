cf.AuxiliaryCoordinate.getattr
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AuxiliaryCoordinate.getattr