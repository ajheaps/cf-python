cf.AuxiliaryCoordinate.hasbounds
================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.AuxiliaryCoordinate.hasbounds