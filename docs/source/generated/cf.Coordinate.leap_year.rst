cf.Coordinate.leap_year
=======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.leap_year