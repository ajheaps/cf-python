cf.CellMethods.has_cellmethod
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMethods.has_cellmethod