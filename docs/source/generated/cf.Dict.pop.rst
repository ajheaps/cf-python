cf.Dict.pop
===========

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Dict.pop