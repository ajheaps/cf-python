cf.Coordinate.getprop
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.getprop