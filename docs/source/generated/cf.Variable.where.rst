cf.Variable.where
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.where