cf.Coordinate.copy
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.copy