cf.Domain.new_measure_identifier
================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.new_measure_identifier