cf.Coordinate.asdimension
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.asdimension