cf.FieldList.flag_meanings
==========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.FieldList.flag_meanings