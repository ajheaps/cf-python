cf.AncillaryVariables.hasprop
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.hasprop