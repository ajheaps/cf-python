cf.Variable.copy
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.copy