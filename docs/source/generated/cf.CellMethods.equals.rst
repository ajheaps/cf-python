cf.CellMethods.equals
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMethods.equals