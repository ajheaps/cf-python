cf.AuxiliaryCoordinate.datum
============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AuxiliaryCoordinate.datum