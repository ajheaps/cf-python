cf.CellMethods.remove
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMethods.remove