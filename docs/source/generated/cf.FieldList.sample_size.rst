cf.FieldList.sample_size
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.sample_size