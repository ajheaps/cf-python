cf.Coordinate.second
====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.second