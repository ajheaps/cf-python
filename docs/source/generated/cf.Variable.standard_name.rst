cf.Variable.standard_name
=========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.standard_name