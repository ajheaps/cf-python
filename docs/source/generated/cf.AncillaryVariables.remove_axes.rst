cf.AncillaryVariables.remove_axes
=================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.remove_axes