cf.FieldList.iscyclic
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.iscyclic