cf.Field.binary_mask
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.binary_mask