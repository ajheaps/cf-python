cf.AncillaryVariables.name
==========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.name