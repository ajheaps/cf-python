cf.AncillaryVariables.axes
==========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.axes