cf.Coordinate.standard_name
===========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.standard_name