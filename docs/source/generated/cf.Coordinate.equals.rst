cf.Coordinate.equals
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.equals