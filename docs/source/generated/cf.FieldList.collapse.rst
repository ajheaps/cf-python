cf.FieldList.collapse
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.collapse