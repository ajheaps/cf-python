cf.Coordinate.where
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.where