cf.Coordinate.add_offset
========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.add_offset