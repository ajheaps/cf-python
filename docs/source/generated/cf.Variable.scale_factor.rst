cf.Variable.scale_factor
========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.scale_factor