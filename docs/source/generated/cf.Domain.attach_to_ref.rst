cf.Domain.attach_to_ref
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.attach_to_ref