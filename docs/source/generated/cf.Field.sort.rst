cf.Field.sort
=============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.sort