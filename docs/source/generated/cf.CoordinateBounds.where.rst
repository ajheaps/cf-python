cf.CoordinateBounds.where
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.where