cf.CoordinateBounds.copy
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.copy