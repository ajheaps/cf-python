cf.CoordinateReference.keys
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateReference.keys