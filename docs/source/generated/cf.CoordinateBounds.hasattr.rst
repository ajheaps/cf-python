cf.CoordinateBounds.hasattr
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.hasattr