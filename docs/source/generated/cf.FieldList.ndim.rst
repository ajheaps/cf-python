cf.FieldList.ndim
=================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.FieldList.ndim