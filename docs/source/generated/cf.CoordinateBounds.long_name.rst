cf.CoordinateBounds.long_name
=============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.long_name