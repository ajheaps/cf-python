cf.CoordinateReference.remove_all_coords
========================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateReference.remove_all_coords