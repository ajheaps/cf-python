cf.FieldList.range
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.range