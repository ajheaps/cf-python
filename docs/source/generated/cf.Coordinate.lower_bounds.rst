cf.Coordinate.lower_bounds
==========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.lower_bounds