cf.AuxiliaryCoordinate.binary_mask
==================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AuxiliaryCoordinate.binary_mask