cf.Dict.popitem
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Dict.popitem