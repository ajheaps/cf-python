cf.AncillaryVariables.hasattr
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.hasattr