cf.Variable.unique
==================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.unique