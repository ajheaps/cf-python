cf.FieldList.remove_items
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.remove_items