cf.Variable.size
================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.size