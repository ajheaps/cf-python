cf.AncillaryVariables.insert_data
=================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.insert_data