cf.CoordinateReference.iterkeys
===============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateReference.iterkeys