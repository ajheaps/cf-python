cf.CoordinateBounds.scale_factor
================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.scale_factor