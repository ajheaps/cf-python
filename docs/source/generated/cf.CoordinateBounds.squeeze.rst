cf.CoordinateBounds.squeeze
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.squeeze