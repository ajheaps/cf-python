cf.Variable.binary_mask
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.binary_mask