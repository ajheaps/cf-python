cf.Coordinate.attributes
========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.attributes