cf.CellMethods.dump
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMethods.dump