cf.Coordinate.unsafe_array
==========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.unsafe_array