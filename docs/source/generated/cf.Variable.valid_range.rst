cf.Variable.valid_range
=======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.valid_range