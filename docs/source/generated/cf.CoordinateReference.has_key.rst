cf.CoordinateReference.has_key
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateReference.has_key