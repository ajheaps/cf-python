cf.Variable.subspace
====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.subspace