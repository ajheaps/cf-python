cf.Field.setattr
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.setattr