cf.Coordinate.data
==================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.data