cf.AuxiliaryCoordinate.setattr
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AuxiliaryCoordinate.setattr