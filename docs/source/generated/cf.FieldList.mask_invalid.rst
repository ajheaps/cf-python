cf.FieldList.mask_invalid
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.mask_invalid