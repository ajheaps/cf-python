cf.AncillaryVariables.unsqueeze
===============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.unsqueeze