cf.FieldList.coords
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.coords