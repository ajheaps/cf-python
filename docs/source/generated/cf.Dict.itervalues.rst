cf.Dict.itervalues
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Dict.itervalues