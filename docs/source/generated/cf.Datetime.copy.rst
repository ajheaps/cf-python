cf.Datetime.copy
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Datetime.copy