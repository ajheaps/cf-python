cf.Variable.data
================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.data