cf.CoordinateReference.popitem
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateReference.popitem