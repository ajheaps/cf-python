cf.Coordinate.cellsize
======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.cellsize