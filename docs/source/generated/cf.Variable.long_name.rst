cf.Variable.long_name
=====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.long_name