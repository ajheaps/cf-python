cf.CoordinateReference.items
============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateReference.items