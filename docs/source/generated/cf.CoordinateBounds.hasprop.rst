cf.CoordinateBounds.hasprop
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.hasprop