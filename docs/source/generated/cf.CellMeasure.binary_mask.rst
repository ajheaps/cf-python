cf.CellMeasure.binary_mask
==========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMeasure.binary_mask