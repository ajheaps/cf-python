cf.Data.geterr
==============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.geterr