cf.List.pop
===========

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.List.pop