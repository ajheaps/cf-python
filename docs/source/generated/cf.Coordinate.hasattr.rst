cf.Coordinate.hasattr
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.hasattr