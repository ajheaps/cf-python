cf.Coordinate.match
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.match