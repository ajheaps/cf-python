cf.Coordinate.identity
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.identity