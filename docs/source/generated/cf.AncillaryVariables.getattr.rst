cf.AncillaryVariables.getattr
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.getattr