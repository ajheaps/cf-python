cf.CoordinateReference.values
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateReference.values