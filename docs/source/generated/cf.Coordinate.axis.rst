cf.Coordinate.axis
==================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.axis