cf.FieldList.binary_mask
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.binary_mask