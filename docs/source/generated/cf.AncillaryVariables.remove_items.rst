cf.AncillaryVariables.remove_items
==================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.remove_items