Home page
=========

* [**cf-python**](http://cfpython.bitbucket.org "cf-python home page")

----------------------------------------------------------------------

Documentation
=============

* [**Online documentation for the latest stable
  release**](http://cfpython.bitbucket.org/docs/latest/ "cf-python HTML
  documentation")

* Online documentation for previous releases: [**cf-python documention
  archive**](http://cfpython.bitbucket.org/docs/archive.html)

* Offline HTML documention for the installed version may be found by
  pointing a browser to ``docs/build/index.html``.


----------------------------------------------------------------------

Dependencies
============

* The package runs on [**Linux**](http://en.wikipedia.org/wiki/Linux)
  and [**Mac OS**](http://en.wikipedia.org/wiki/Mac_OS) operating
  systems.

* Requires a [**python**](http://www.python.org) version from 2.6 up
  to, but not including, 3.0.
 
* Requires the [**python psutil
  package**](https://pypi.python.org/pypi/psutil) at version 0.6.0 or
  newer (the latest version is recommended).

* Requires the [**python numpy
  package**](https://pypi.python.org/pypi/numpy) at version 1.7 or
  newer.

* Requires the [**python matplotlib
  package**](https://pypi.python.org/pypi/matplotlib) at version 1.4.2
  or newer.

* Requires the [**python netCDF4
  package**](https://pypi.python.org/pypi/netCDF4) at version 0.9.7 or
  newer (the latest version is recommended). This package requires the
  [**netCDF**](http://www.unidata.ucar.edu/software/netcdf),
  [**HDF5**](http://www.hdfgroup.org/HDF5) and
  [**zlib**](ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4)
  libraries.

* Requires the [**UNIDATA Udunits-2
  library**](http://www.unidata.ucar.edu/software/udunits). This is a
  C library which provides support for units of physical
  quantities. If The Udunits-2 shared library file
  (``libudunits2.so.0`` on Linux or ``libudunits2.0.dylibfile`` on Mac
  OS) is in a non-standard location then its path should be added to
  the ``LD_LIBRARY_PATH`` environment variable.


----------------------------------------------------------------------

Visualisation
=============

* The [**cfplot package**](https://pypi.python.org/pypi/cfplot) at
  version 1.7.5 or newer provides metadata-aware visualisation for
  cf-python fields. This is not a dependency for cf-python.


----------------------------------------------------------------------

Installation
============

To install from [**PyPI**](https://pypi.python.org/pypi/cf-python):

    pip install cf-python

Alternatively, to install from source:

1. Download the cf package from [**cf-python
   downloads**](https://bitbucket.org/cfpython/cf-python/downloads).
  
2. Unpack the library:
  
        tar zxvf cf-python-1.0.3.tar.gz
        cd cf-python-1.0.3

3. Install the package:
          
    * To install the cf package to a central location:
       
           python setup.py install
       
    * To install the cf package locally to the user in a default
      location:

           python setup.py install --user
      
    * To install the cf package in the ``<directory>`` of your
      choice:
      
           python setup.py install --home=<directory>

----------------------------------------------------------------------

Tests
=====

The test script is in the ``test`` directory:

    python test/run_tests.py


----------------------------------------------------------------------

Command line utilities
======================

The ``cfdump`` tool generates text representations on standard output
of the CF fields contained in the input files. 

The ``cfa`` tool creates and writes to disk the CF fields contained in
the input files.

During the installation described above, these scripts will be copied
automatically to a location given by the ``PATH`` environment
variable.

For usage instructions, use the ``-h`` option to display the manual
pages:

    cfdump -h
    cfa -h

----------------------------------------------------------------------

Code license
============

[**MIT License**](http://opensource.org/licenses/mit-license.php)

  * Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use, copy,
    modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

  * The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
