from inspect import getmro
from re      import findall, sub
'''
Common docstring parameter templates
'''
_doc_parameter_template = {

    # ----------------------------------------------------------------
    '+item_selection': '''{Items} are selected with the criteria specified by the keyword
parameters. By default, if multiple criteria are given then the
{items} will be the intersection of each keyword's selection (see the
*match_and* parameter). If no keyword parameters are specified then
all {items} are selected.''',

    # ----------------------------------------------------------------
    '+ndim': '''ndim : *optional*
        Select the {items} whose number of data array axes satisfy the
        given condition. A range of data numbers of array axes may be
        selected if *ndim* is a `cf.Query` object.

          *Example:*
            ``ndim=1`` selects {items} which span exactly one axis and
            ``ndim=cf.ge(2)`` selects {items} which span two or more
            axes (see `cf.ge`).''',

    # ----------------------------------------------------------------
    '+axes': '''axes : *optional*
        Select {items} whose data array spans at least one of the
        specified axes, taken in any order, and possibly others. The
        axes are those that would be selected by this call of the
        field's `~cf.Field.axes` method: ``f.axes(axes)`` or, if
        *axes* is a dictionary of parameters to the `~cf.Field.axes`
        method, ``f.axes(**axes)``. See `cf.Field.axes` for details.

          *Example:*
            To select {items} which span a time axis, and possibly
            others, you could set: ``axes='T'``.

          *Example:*
            To select {items} which span a latitude and/or longitude
            axes, and possibly others, you could set: ``axes=['X',
            'Y']``.

          *Example:*
            To specify axes with size 19 you could set
            ``axes={{'size': 19}}``. To specify depth axes with size
            40 or more, you could set: ``axes={{'axes': 'depth',
            'size': cf.ge(40)}}`` (see `cf.ge`).''',

    # ----------------------------------------------------------------
    '+axes_subset': '''axes_subset : *optional* 
        Select {items} whose data array spans all of the specified
        axes, taken in any order, and possibly others. The axes are
        those that would be selected by this call of the field's
        `~cf.Field.axes` method: ``f.axes(axes)`` or, if *axes* is a
        dictionary of parameters to the `~cf.Field.axes` method,
        ``f.axes(**axes)``. See `cf.Field.axes` for details.

          *Example:*
            To select {items} which span a time axes, and possibly
            others, you could set: ``axes='T'``.

          *Example:*
            To select {items} which span a latitude and/or longitude
            axes, and possibly others, you could set: ``axes=['X',
            'Y']``.

          *Example:*
            To specify axes with size 19 you could set
            ``axes={{'size': 19}}``. To specify depth axes with size
            40 or more, you could set: ``axes={{'axes': 'depth',
            'size': cf.ge(40)}}`` (see `cf.ge`).''',

    # ----------------------------------------------------------------
    '+axes_superset': '''axes_superset : *optional*
        Select {items} whose data array spans a subset of the
        specified axes, taken in any order, and no others. The axes
        are those that would be selected by this call of the field's
        `~cf.Field.axes` method: ``f.axes(axes)`` or, if *axes* is a
        dictionary of parameters to the `~cf.Field.axes` method,
        ``f.axes(**axes)``. See `cf.Field.axes` for details.

          *Example:*
            To select {items} which span a time axis, and no others,
            you could set: ``axes='T'``.

          *Example:*
            To select {items} which span latitude and/or longitude
            axes, and no others, you could set: ``axes=['X', 'Y']``.

          *Example:*
            To specify axes with size 19 you could set
            ``axes={{'size': 19}}``. To specify depth axes with size
            40 or more, you could set: ``axes={{'axes': 'depth',
            'size': cf.ge(40)}}`` (see `cf.ge`).''',


    # ----------------------------------------------------------------
    '+axes_all': '''axes_all : *optional*
        Select {items} whose data array spans all of the specified
        axes, taken in any order, and no others. The axes are those
        that would be selected by this call of the field's
        `~cf.Field.axes` method: ``f.axes(axes)`` or, if *axes* is a
        dictionary of parameters to the `~cf.Field.axes` method,
        ``f.axes(**axes)``. See `cf.Field.axes` for details.

          *Example:*
            To select {items} which span a time axis, and no others,
            you could set: ``axes='T'``.

          *Example:*
            To select {items} which span latitude and longitude axes,
            and no others, you could set: ``axes=['X', 'Y']``.

          *Example:*
            To specify axes with size 19 you could set
            ``axes={{'size': 19}}``. To specify depth axes with size
            40 or more, you could set: ``axes={{'axes': 'depth',
            'size': cf.ge(40)}}`` (see `cf.ge`).''',

    # ----------------------------------------------------------------
    '+role': '''role : (sequence of) str, optional
        Select items of the given roles. Valid roles are:

           =======  ============================
           role     Items selected
           =======  ============================
           ``'d'``  Dimension coordinate objects
           ``'a'``  Auxiliary coordinate objects
           ``'m'``  Cell measure objects
           ``'r'``  Coordinate reference objects
           =======  ============================

        Multiple roles may be specified by a multi-character string or
        a sequence.

          *Example:*
            Selecting auxiliary coordinate and cell measure objects
            may be done with any of the following values of *role*:
            ``'am'``, ``'ma'``, ``('a', 'm')``, ``['m', 'a']``,
            ``set(['a', 'm'])``, etc.''',


    # ----------------------------------------------------------------
    '+exact': '''exact : bool, optional
        The *exact* parameter applies to the interpretation of
        string-valued conditions given by the *items* parameter. By
        default *exact* is False, which means that:

          * A string is treated as a regular expression understood by
            the :py:obj:`re` module and {a} {item} is
            selected if its corresponding value matches the regular
            expression using the `re.match` method.

          * Units and calendar strings are evaluated for equivalence
            rather then equality (e.g. ``'metre'`` is equivalent to
            ``'m'`` and to ``'km'``).
 
        ..

          *Example:*
            To select {items} with with any units of pressure:
            ``f.{name}('units:hPa')``. To select {items} with a
            standard name which begins with "air" and with any units
            of pressure: ``f.{name}({{'standard_name': 'air', 'units':
            'hPa'}})``.

        If *exact* is True then:

          * A string is not treated as a regular expression and {a}
            {item} is selected if its corresponding value equals the
            string.

          * Units and calendar strings are evaluated for exact equality
            rather than equivalence (e.g. ``'metre'`` is equal to
            ``'m'``, but not to ``'km'``).

        ..

          *Example:*
            To select {items} with with units of hectopascals but not,
            for example, Pascals: ``f.{name}('units:hPa',
            exact=True)``. To select {items} with a standard name of
            exactly "air_pressure" and with units of exactly
            hectopascals: ``f.{name}({{'standard_name':
            'air_pressure', 'units': 'hPa'}}, exact=True)``.

        Note that `cf.Query` objects provide a mechanism for
        overriding the *exact* parameter for individual values.

          *Example:*
            ``f.{name}({{'standard_name': cf.eq('air', exact=False),
            'units': 'hPa'}}, exact=True)`` will select {items} with a
            standard name which begins "air" but with units of exactly
            hectopascals (see `cf.eq`).

          *Example:*
            ``f.{name}({{'standard_name': cf.eq('air_pressure'),
            'units': 'hPa'}})`` will select {items} with a standard name
            of exactly "air_pressure" but with any units of pressure
            (see `cf.eq`).''',

    # ----------------------------------------------------------------
    '+match_and': '''match_and : bool, optional
        By default *match_and* is True and {items} are selected if
        they satisfy the all of the specified conditions.

        If *match_and* is False then {items} are selected if they
        satisfy at least one of the specified conditions.

          *Example:*
            To select {items} with identity beginning with "ocean"
            **and** two data array axes: ``f.{name}('ocean',
            ndim=2)``.
        
          *Example:*
            To select {items} with identity beginning with "ocean"
            **or** two data array axes: ``f.{name}('ocean', ndim=2,
            match_and=False)``.''',

    # ----------------------------------------------------------------
    '+inverse': '''inverse : bool, optional
        If True then select {items} other than those selected by all
        other criteria.''',

    # ----------------------------------------------------------------
    '+copy': '''copy : bool, optional
        If True then a returned {item} is a copy. By default it is not
        copied, so in-place changes are stored in the domain.''',

    # ----------------------------------------------------------------
    '+key': '''key : bool, optional
        If True then return the domain's identifier for the selected
        {item}, rather than the {item} itself.''',

    # ----------------------------------------------------------------
    '+items': '''items : *optional*
        Select {items} whose properties or attributes satisfy the
        given conditions. The *items* parameter may be one, or a
        sequence, of:

          * `None` or an empty dictionary. All {items} are
            selected. This is the default.

       ..

          * A string specifying one of the CF coordinate types:
            ``'T'``, ``'X'``, ``'Y'`` or ``'Z'``. {A} {item} has an
            attribute for each coordinate type and is selected if the
            attribute for the specified type is True.
          
              *Example:*
                To select CF time {items}: ``items='T'``.

        ..

          * A string which identifies {items} based on their
            string-valued metadata. The value may take one of the
            following forms:

              ==============  ========================================
              Value           Interpretation
              ==============  ========================================
              Contains ``:``  Selects on the CF property specified
                              before the first ``:``
                               
              Contains ``%``  Selects on the attribute specified
                              before the first ``%``
              
              Anything else   Selects on identity as returned by the
                              {item}'s `{identity}` method
              ==============  ========================================

            By default the part of the string to be compared with an
            {item} is treated as a regular expression understood by the
            :py:obj:`re` module and {a} {item} is selected if its
            appropriate value matches the regular expression using the
            :py:obj:`re.match` method (i.e. if zero or more characters
            at the beginning of {item}'s value match the regular
            expression pattern). See the *exact* parameter for
            details.

              *Example:*
                To select {items} with standard names which begin "lat":
                ``items='lat'``.

              *Example:*
                To select {items} with long names which begin "air":
                ``items='long_name:air'``.

              *Example:*
                To select {items} with netCDF variable names which begin
                "lon": ``items='ncvar%lon'``.

              *Example:*
                To select {items} with identities which end with the
                letter "z": ``items='.*z$'``.

              *Example:*
                To select {items} with long names which start with the
                string ".*a": ``items='long_name%\.\*a'``.
        ..

          * A dictionary which identifies properties of the {items} with
            corresponding tests on their values. {A} {item} is selected
            if **all** of the tests in the dictionary are passed.

            In general, each dictionary key is a CF property name with
            a corresponding value to be compared against the {item}'s CF
            property value.
            
            If the dictionary value is a string then by default it is
            treated as a regular expression understood by the
            :py:obj:`re` module and {a} {item} is selected if its
            appropriate value matches the regular expression using the
            :py:obj:`re.match` method (i.e. if zero or more characters
            at the beginning of {item}'s value match the regular
            expression pattern). See the *exact* parameter for
            details.
            
              *Example:*
                To select {items} with standard name of exactly
                "air_temperature" and long name beginning with the
                letter "a": ``items={{'standard_name':
                cf.eq('air_temperature'), 'long_name': 'a'}}`` (see
                `cf.eq`).

            Some key/value pairs have a special interpretation:

              ==================  ====================================
              Special key         Value
              ==================  ====================================
              ``'units'``         The value must be a string and by
                                  default is evaluated for
                                  equivalence, rather than equality,
                                  with {a} {item}'s `units` property,
                                  for example a value of ``'Pa'``
                                  will match units of Pascals or
                                  hectopascals, etc. See the *exact*
                                  parameter.
                            
              ``'calendar'``      The value must be a string and by
                                  default is evaluated for
                                  equivalence, rather than equality,
                                  with {a} {item}'s `calendar`
                                  property, for example a value of
                                  ``'noleap'`` will match a calendar
                                  of noleap or 365_day. See the
                                  *exact* parameter.

              `None`              The value is interpreted as for a
                                  string value of the *items*
                                  parameter. For example,
                                  ``items={{None: 'air'}}`` is
                                  equivalent to ``items='air'``,
                                  ``items={{None: 'ncvar%pressure'}}``
                                  is equivalent to
                                  ``items='ncvar%pressure'`` and
                                  ``items={{None: 'Y'}}`` is equivalent
                                  to ``items='Y'``.
              ==================  ====================================

              *Example:*
                To select {items} with standard name starting with
                "air", units of temperature and a netCDF variable name
                of "tas" you could set
                ``items={{'standard_name': 'air', 'units': 'K', None:
                'ncvar%tas$'}}``.

       ..

          * A domain item identifier (such as ``'dim1'``, ``'aux0'``,
            ``'msr2'``, ``'ref0'``, etc.). Selects the corresponding
            {item}.  

              *Example:*
                To select the {item} with domain identifier "dim1":
                ``items='dim1'``.

        If *items* is a sequence of any combination of the above then
        the selected {items} are the union of those selected by each
        element of the sequence. If the sequence is empty then no
        {items} are selected.''',

    # ----------------------------------------------------------------
    '+axes, kwargs': '''axes, kwargs : *optional*
        Select axes. The *axes* parameter may be one, or a sequence,
        of:

          * `None`. If there no *kwargs* arguments have been set
            then selects all axes.

        ..

          * An integer or :py:obj:`slice` object. Explicitly selects
            the axes corresponding to the given position(s) in the
            list of axes of the field's data array.

              *Example:*
                To select the third data array axis (counting from the
                left): ``axes=2``.

              *Example:*
                To select the last axis (counting from the left):
                ``axes=-1``.

              *Example:* 
                To select the last three data array axes (counting from
                the left): ``axes=slice(-3, None)``

        ..

          * A domain axis identifier. Explicitly selects this axis.

              *Example:*
                To select axis "dim1": ``axes='dim1'``.

        ..

          * Any value accepted by the *items* parameter of the field's
            `items` method. Used in conjunction with the *kwargs*
            parameters to select the axes which span the items that
            would be identified by this call of the field's `items`
            method: ``f.items(items=axes, axes=None, **kwargs)``. See
            `cf.Field.items` for details.

              *Example:*
                To select the axes spanned by one dimensionsal time
                coordinates: ``f.{name}('T', ndim=1)``.

        If *axes* is a sequence of any combination of the above then
        the selected axes are the union of those selected by each
        element of the sequence. If the sequence is empty then no axes
        are selected.''',

    # ----------------------------------------------------------------
    '+size': '''size : *optional*
        Select axes whose sizes equal *size*. Axes whose sizes lie
        within a range sizes may be selected if *size* is a `cf.Query`
        object.

          *Example:*        
            ``size=1`` selects size 1 axes.

          *Example:*
            ``size=cf.ge(2)`` selects axes with sizes greater than 1
            (see `cf.ge`).''',

    # ----------------------------------------------------------------
    '+iscyclic': '''iscyclic : bool, optional        
        If False then the axis is set to be non-cyclic. By default the
        selected axis is set to be cyclic.''',

    # ----------------------------------------------------------------
    '+period': '''period : data-like, optional       
        Set the period for a dimension coordinate object which spans
        the selected axis. The absolute value of *period* is used.''',

    # ----------------------------------------------------------------
    '+i': '''i : bool, optional
        If True then update the variable in place. By default a new
        variable is created. In either case, a variable is
        returned.''',

    # ----------------------------------------------------------------
    '+def_aux': '''

Return {a} {item} of the domain, or its domain identifier.

{+item_selection}

If no unique {item} can be found then `None` is returned.

To find multiple {items}, use the `{name}s` method.

Note that ``f.{name}(inverse=False, **kwargs)`` is equivalent to
``f.item(role='a', inverse=False, **kwargs)``.

.. seealso:: `auxs`, `measure`, `coord`, `ref`, `dim`, `item`,
             `remove_item`

:Examples 1:

A latitude {item} could potentially be selected with any of:

>>> a = f.{name}('Y')
>>> a = f.{name}('latitude')
>>> a = f.{name}('long_name:latitude')
>>> a = f.{name}('aux1')
>>> a = f.{name}(axes_all='Y')

:Parameters:

    {+items}

    {+ndim}

    {+axes}

    {+axes_all}

    {+axes_subset}

    {+axes_superset}

    {+match_and}

    {+exact}
       
    {+inverse}

    {+key}
        
    {+copy}''',

    # ----------------------------------------------------------------
    '+def_axes_sizes': '''

Return the sizes of domain axes.

.. seealso::  `axis`, `axis_size`, `axis_identity`, `axis_name`

:Examples 1:

Find the sizes of all domain axes:

>>> a = f.axes_sizes()

Find the size of the X axis:

>>> a = f.axes_sizes('X')

:Parameters:

    {+axes, kwargs}

    {+size}
    
    key : bool, optional
        In the output dictionary, identify axes by domain identifier,
        rather than by name.''',

    # ----------------------------------------------------------------
    '+def_axis': '''

Return a domain axis identifier.

.. versionadded:: 1.0

.. seealso:: `axes`, `axis_name`, `axis_size`, `remove_axes`

:Examples 1:


:Parameters:

    {+axes, kwargs}

    {+size}''',

    # ----------------------------------------------------------------
    '+def_ceil': '''

Return a variable with the ceiling of the data.

The ceiling of the scalar ``x`` is the smallest integer ``i``, such
that ``i >= x``.

.. versionadded:: 1.0

.. seealso:: `floor`, `rint`, `trunc`

:Examples 1:

Create a new variable with the ceiling of the data:

>>> g = f.ceil()

:Parameters:

    {+i}

:Returns:

    out : 
        A variable with the new data.''',

    # ----------------------------------------------------------------
    '+def_cyclic': '''

Set the cyclicity of an axis.

.. versionadded:: 1.0

.. seealso:: `axis`, `iscyclic`, `period`, `setcyclic`''',

    # ----------------------------------------------------------------
    '+def_dim': '''

Return {a} {item} of the domain, or its domain identifier.

{+item_selection}

If no unique {item} can be found then `None` is returned.

To find multiple {items}, use the `{name}s` method.

Note that ``f.{name}(inverse=False, **kwargs)`` is equivalent to
``f.item(role='d', inverse=False, **kwargs)``.

.. seealso:: `aux`, `measure`, `coord`, `ref`, `dims`, `item`,
             `remove_item`

:Examples 1:

A latitude {item} could potentially be selected with any of:

>>> d = f.{name}('Y')
>>> d = f.{name}('latitude')
>>> d = f.{name}('long_name:latitude')
>>> d = f.{name}('dim1')
>>> d = f.{name}(axes_all='Y')''',

    # ----------------------------------------------------------------
    '+def_floor': '''

Return a variable with the floor of the data.

The floor of the scalar ``x`` is the largest integer ``i``, such that
``i <= x``.

.. versionadded:: 1.0

.. seealso:: `ceil`, `rint`, `trunc`

:Examples 1:

Create a new variable with the floor of the data:

>>> g = f.floor()

:Parameters:

    {+i}

:Returns:

    out : 
        A variable with the new data.''',

    # ----------------------------------------------------------------
    '+def_iscyclic': '''

Whether or not a particular axis is cyclic.

.. versionadded:: 1.0

.. seealso:: `axis`, `cyclic`, `period`''',

    # ----------------------------------------------------------------
    '+def_measure': '''

Return {a} {item} of the domain, or its domain identifier.

{+item_selection}

If no unique {item} can be found then `None` is returned.

To find multiple {items}, use the `{name}s` method.

Note that ``f.{name}(inverse=False, **kwargs)`` is equivalent to
``f.item(role='m', inverse=False, **kwargs)``.

.. seealso:: `aux`, `coord`, `dim`, `item`, `measures`, `ref`,
             `remove_item`

:Examples 1:

An area {item} could potentially be selected with any of:

>>> c = f.{name}('area')
>>> c = f.{name}('units:metre2')
>>> c = f.{name}('msr1')''',

    # ----------------------------------------------------------------
    '+def_ref': '''

Return {a} {item} of the domain, or its domain identifier.

{+item_selection}

If no unique {item} can be found then `None` is returned.

To find multiple {items}, use the `{name}s` method.

Note that ``f.{name}(inverse=False, **kwargs)`` is equivalent to
``f.item(role='r', inverse=False, **kwargs)``.

.. seealso:: `aux`, `measure`, `coord`, `refs`, `dim`, `item`,
             `remove_item`

:Examples 1:

A rotated latitude longitude {item} could be selected with:

>>> c = f.{name}('rotated_latitude_longitude')''',

    # ----------------------------------------------------------------
    '+def_rint': '''

Return a variable with rounded data.

The scalar ``x`` is rounded to the nearest integer ``i``.

.. versionadded:: 1.0

.. seealso:: `ceil`, `floor`, `trunc`

:Examples 1:

Create a new variable with rounded data:

>>> g = f.rint()

:Parameters:

    {+i}

:Returns:

    out : 
        A variable with the new data.''',

    # ----------------------------------------------------------------
    '+def_trunc': '''

Return a variable with truncated data.

The truncated value the scalar ``x``, is the nearest integer ``i``
which is closer to zero than ``x`` is. I.e. the fractional part
of the signed number ``x`` is discarded.

.. versionadded:: 1.0

.. seealso:: `ceil`, `floor`, `rint`

:Examples 1:

Create a new variable with truncated data:

>>> g = f.trunc()

:Parameters:

    {+i}

:Returns:

    out :
        A variable with the new data.''',

}

for n in (0, 1):
    # Do this twice to catch all of the references
    for key, value in _doc_parameter_template.items():
        for key2, value2 in _doc_parameter_template.items():
            x = '{'+key2+'}'
            if x in value:
#                if key == '+def_aux':
#                    print key, x, value2[:10]
                _doc_parameter_template[key] = value.replace(x, value2)
#--- End: for

def format_docstring(f, **kwargs):
    '''

:Parameter:

    f : function

    kwargs : *optional*

:Returns:

    None

:Examples:

'''
    # Set {name}
    kwargs['name'] = kwargs.get('name', f.func_name)

    # Set {items}
    item = kwargs.get('item', None)
    if item is not None and 'items' not in kwargs:
        if item[-1] == 'y':
            kwargs['items'] = item[:-1] + 'ies'
        else:
            kwargs['items'] = item + 's'
    #--- End: if

    # Set {a} and {A}: the indefinite article
    if item is not None:
        if item[0] in 'aeiouhAEIOUH':
            kwargs['a'] = 'an'
            kwargs['A'] = 'An'
        else:
            kwargs['a'] = 'a'
            kwargs['A'] = 'A'
    #--- End: if

    # Set {Item}
    item = kwargs.get('item', None)
    if item is not None:
        kwargs['Item'] = kwargs.get('Item', item[:1].upper() + item[1:])

    # Set {Items}
    items = kwargs.get('items', None)
    if items is not None:
        kwargs['Items'] = kwargs.get('Items', items[:1].upper() + items[1:])

    kwargs2 = {}
    for arg in set(findall('{\+.*?}', f.__doc__)):
        arg = sub('[{}]', '' ,arg)
#        if arg == '+def_aux':
#            print arg, kwargs , _doc_parameter_template[arg]
        kwargs2[arg] = _doc_parameter_template[arg].format(**kwargs)
    #--- End: for

    f.__doc__ = f.__doc__.format(**kwargs2)
#--- End: def
